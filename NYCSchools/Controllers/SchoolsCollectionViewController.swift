//
//  SchoolsCollectionViewController.swift
//  NYCSchools
//
//  Created by Sai Madhukar Somu on 6/16/22.
//

import Foundation
import UIKit

class SchoolsCollectionViewController: UICollectionViewController {
    
    @IBOutlet weak var emptyView: UIView!
    private var viewModel: SchoolsViewModel = SchoolsViewModel()
    private var schools: [School] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        transparentNavBar()
        fetchAllSchools()

       configureCollectionView()
    }
    
    /// EmptyView Refersh button - To fetch the data again
    @IBAction func refresh() {
        fetchAllSchools()
    }
    
    private func fetchAllSchools() {
        viewModel.fetchSchoolCount { result in
            self.fetchData()
        } onFailure: {
            self.emptyView.isHidden = false
        }
    }
    
    ///  Fetching the data using viewModel
    @objc private func fetchData() {
        
        viewModel.fetchSchools { data in
            self.schools = data
            DispatchQueue.main.async {
                self.emptyView.isHidden = true
                self.collectionView.reloadData()
            }
        } onFailure: {
            self.schools = []
        }
    }
    
    private func configureCollectionView() {
        collectionView.register(HeaderView.self,forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerView")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.prefetchDataSource = self
        collectionView.isPagingEnabled = true
        collectionView.reloadData()
    }
}

extension SchoolsCollectionViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSourcePrefetching {
    
    /// Sets the header for the collecgtion view
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
                                    withReuseIdentifier: "headerView",
                                    for: indexPath)
        
        return headerView
    }
    
    /// Size of the Header
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.bounds.width, height: 100)
    }
    
    
    /// Enter the number of Sections in collection view
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    /// Number of cells in each section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.schoolCount
    }
    
    
    /// Cell for row at indexPath
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let school = schools[indexPath.row]
        let randomGradient = BestGradients.top15.randomElement() ?? BestGradients.top15[0]
        
        if let schoolCell = collectionView.dequeueReusableCell(withReuseIdentifier: "school", for: indexPath) as? SchoolCell {
            schoolCell.setGradientBackground(value: randomGradient)
            schoolCell.updateUI(school: school)
            return schoolCell
        }
        return UICollectionViewCell()
    }
    
    /// Will display provides the next cells that are going to be dequeued and displayed on the screens
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let rotation = CATransform3DTranslate(CATransform3DIdentity, -500, 10, 0)
        cell.layer.transform = rotation

        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut) {
            cell.layer.transform = CATransform3DIdentity
        }
    }
        
    ///  DidSelect provides the action handler for TapGesture performed on cells
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedSchool = schools[indexPath.row]
        if let detailedVC = storyboard?.instantiateViewController(withIdentifier: "detailedVC") as? SchoolsDetailedViewController {
            detailedVC.school = selectedSchool
            detailedVC.viewModel = viewModel
            detailedVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            navigationController?.pushViewController(detailedVC, animated: false)
        }
    }
    
    ///  Setting the exact size of each cell WIDTH and HEIGHT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
        return CGSize(width: self.view.bounds.width * 0.48, height: self.view.bounds.height * 0.30)
    }
    
    
    ///  Prefetching the data to keep the data efficienent and easy to display
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        
        guard let prefetchIndex = indexPaths.first else { return }
        if prefetchIndex.row + 25  > schools.count {
            print("Prefixing index: ", prefetchIndex)
            print("School Count: ", schools.count)
            viewModel.fetchSchools(offSet: prefetchIndex.row + 25 ) { schools in
                self.schools.append(contentsOf: schools)
                collectionView.reloadItems(at: indexPaths)
            } onFailure: {
                print("Failed to prefetch data")
            }
        }
    }
}
