//
//  SchoolsDetailedViewController.swift
//  NYCSchools
//
//  Created by Sai Madhukar Somu on 6/16/22.
//

import MapKit
import UIKit

class SchoolsDetailedViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var testsTaken: UILabel!
    @IBOutlet weak var readingAvgScore: UILabel!
    @IBOutlet weak var mathsAvgScore: UILabel!
    @IBOutlet weak var writingAvgScore: UILabel!
    
    var school: School?
    var viewModel: SchoolsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        enableDarkMode()
        configureMapView()
        fetchData()
        presentPopupView()
    }
    
    private func fetchData() {
        
        guard let school = school, let viewModel = viewModel else {
            return
        }
        
        viewModel.fetchSATScores(schoolDBN: school.dbn, onSuccess: { score in
            DispatchQueue.main.async {
                self.updateUI(sat: score)
            }
        }, onFailure: {
            print("Failed to Fetch SAT Scores")
        })
    }
    
    private func presentPopupView() {
        
        bottomView.setGradientBackground(value: BestGradients.top15.randomElement()!)
        bottomView.layer.cornerRadius = 20
        bottomView.layer.masksToBounds = true
    }
    
    private func configureMapView() {
        
        /// setting the region based on coordinates
        guard let school = school else { return }
        let latitude = Double(school.latitude) ?? .zero
        let longitude =  Double(school.longitude) ?? .zero
        let schoolCoordinates = CLLocation(latitude: latitude, longitude: longitude)
        
        let coordinateRegion = MKCoordinateRegion( center: schoolCoordinates.coordinate,
                                                   latitudinalMeters: 1000,
                                                   longitudinalMeters: 1000)
        mapView.setRegion(coordinateRegion, animated: true)
        
        /// Adding PIN annotation to maps
        let artwork = MKPointAnnotation()
        artwork.title = school.name
        artwork.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        mapView.addAnnotation(artwork)
    }
    
    private func updateUI(sat: SATScore) {
        name.text = sat.schoolName
        testsTaken.text = sat.numOfTestTakers
        mathsAvgScore.text = sat.mathAvgScore
        readingAvgScore.text = sat.readingAvgScore
        writingAvgScore.text = sat.writingAvgScore
    }
}

private extension SchoolsDetailedViewController {
    
    func enableDarkMode() {
        if #available(iOS 13.0, *) {
            self.mapView.overrideUserInterfaceStyle = .dark
        }
    }
}
