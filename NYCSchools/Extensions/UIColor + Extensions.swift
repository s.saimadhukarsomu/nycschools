//
//  UIColor + Extensions.swift
//  NYCSchools
//
//  Created by Sai Madhukar Somu on 6/16/22.
//

import UIKit

/// UIColor extension for using the hexValues
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}


/// Gradient Struct with 2 colors
public struct Gradients {
    let colorX: UIColor
    let colorY: UIColor
}

/// Utility Class to provide the best UI expereience
class BestGradients {
    
    static var top15: [Gradients] = [
        
        Gradients(colorX: UIColor(hexString: "#ffafbd"), colorY: UIColor(hexString: "#ffc3a0")),
        
        Gradients(colorX: UIColor(hexString: "#2193b0"), colorY: UIColor(hexString: "#6dd5ed")),
        
        Gradients(colorX: UIColor(hexString: "#cc2b5e"), colorY: UIColor(hexString: "#753a88")),
        
        Gradients(colorX: UIColor(hexString: "#ee9ca7"), colorY: UIColor(hexString: "#ffdde1")),
        
        Gradients(colorX: UIColor(hexString: "#42275a"), colorY: UIColor(hexString: "#734b6d")),
        
        
        Gradients(colorX: UIColor(hexString: "#bdc3c7"), colorY: UIColor(hexString: "#2c3e50")),
        
        Gradients(colorX: UIColor(hexString: "#de6262"), colorY: UIColor(hexString: "#ffb88c")),
        
        Gradients(colorX: UIColor(hexString: "#06beb6"), colorY: UIColor(hexString: "#48b1bf")),
        
        Gradients(colorX: UIColor(hexString: "#eb3349"), colorY: UIColor(hexString: "#f45c43")),
        
        Gradients(colorX: UIColor(hexString: "#dd5e89"), colorY: UIColor(hexString: "#f7bb97")),
        
        
        
        Gradients(colorX: UIColor(hexString: "#614385"), colorY: UIColor(hexString: "#516395")),
        
        Gradients(colorX: UIColor(hexString: "#eecda3"), colorY: UIColor(hexString: "#ef629f")),
        
        Gradients(colorX: UIColor(hexString: "#eacda3"), colorY: UIColor(hexString: "#d6ae7b")),
        
        Gradients(colorX: UIColor(hexString: "#02aab0"), colorY: UIColor(hexString: "#00cdac")),
        
        Gradients(colorX: UIColor(hexString: "#000428"), colorY: UIColor(hexString: "#004e92")),
    ]
}
