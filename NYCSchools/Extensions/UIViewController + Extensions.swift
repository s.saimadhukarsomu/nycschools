//
//  UIViewController + Extensions.swift
//  NYCSchools
//
//  Created by Sai Madhukar Somu on 6/16/22.
//

import Foundation
import UIKit


extension UIViewController {
    
    func transparentNavBar() {
        let app = UINavigationBarAppearance()
        app.configureWithTransparentBackground()
        self.navigationController?.navigationBar.standardAppearance = app
        self.navigationController?.navigationBar.scrollEdgeAppearance = app
        self.navigationController?.navigationBar.compactAppearance = app
    }
}


    
extension UIView {
    
    func setGradientBackground(value: Gradients) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [value.colorX.cgColor, value.colorY.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        layer.insertSublayer(gradientLayer, at: 0)
    }
}

