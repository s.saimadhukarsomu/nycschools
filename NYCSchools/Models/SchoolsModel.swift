//
//  SchoolsModel.swift
//  NYCSchools
//
//  Created by Sai Madhukar Somu on 6/16/22.
//

import Foundation

/// Main School Object Model with Decodable and Codingkeys
struct School: Decodable {
    let dbn: String
    let name: String
    let phone: String
    let address1: String
    let city: String
    let zip: String
    let state: String
    let borough: String
    
    let latitude: String
    let longitude: String
    
    enum CodingKeys: String, CodingKey {
        
        case dbn = "dbn"
        case name = "school_name"
        case phone = "phone_number"
        case address1 = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case state = "state_code"
        case borough = "borough"
        case latitude = "latitude"
        case longitude = "longitude"
    }
}


/// SATScore Object Model with Decodable and CodingKeys
struct SATScore: Decodable {
    
    let dbn: String
    let schoolName: String
    let numOfTestTakers: String
    let readingAvgScore: String
    let mathAvgScore: String
    let writingAvgScore: String
    
    enum CodingKeys: String, CodingKey {
        
        case dbn = "dbn"
        case schoolName = "school_name"
        case numOfTestTakers = "num_of_sat_test_takers"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}
