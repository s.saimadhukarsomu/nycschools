//
//  SchoolsViewModel.swift
//  NYCSchools
//
//  Created by Sai Madhukar Somu on 6/16/22.
//
import Foundation
import Alamofire

/// SchoolsViewModel contains all the business logic to fetch all the data
public class SchoolsViewModel {
    
    var schoolCount = 0

    /// fetchSchools gets all the data from the open source city of newyourk
    /// offSet value is optional
    func fetchSchools(offSet: Int = 0,onSuccess success:@escaping([School]) -> Void, onFailure failure:@escaping () -> Void) {
       
       let request = AF.request("https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=25&$offset=\(offSet)", method: .get, parameters: nil, encoding: JSONEncoding.default)
       request.responseDecodable(of: [School].self) { response in
           if let status = response.response?.statusCode {
                   switch(status){
                   case 200:
                       print("Success")
                   default:
                       print("error with response status: \(status)")
                   }
           }
           if let result = response.value {
               success(result)
           }
       }
   }
    
    
    /// making this call to get the total number of schools in total - Useful in loading the collection view
    func fetchSchoolCount(onSuccess success:@escaping(Int) -> Void, onFailure failure:@escaping() -> Void) {
        let request = AF.request("https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=100", method: .get, parameters: nil, encoding: JSONEncoding.default)
        request.responseDecodable(of: [School].self) { response in
            if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        print("Success School Count")
                    default:
                        print("error with response status: \(status)")
                    }
            }
            if let result = response.value {
                self.schoolCount = result.count
                success(result.count)
            }
        }
    }
    
    
    /// Data call to fetch the specificc SAT score of the High school using the DBN number
    func fetchSATScores(schoolDBN: String, onSuccess success:@escaping (SATScore) -> Void, onFailure failure:@escaping () -> Void) {
        
        let request = AF.request("https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=" + schoolDBN, method: .get, parameters: nil, encoding: JSONEncoding.default)
        request.responseDecodable(of: [SATScore].self) { response in
            if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        print("Success")
                    default:
                        print("error with response status: \(status)")
                        failure()
                        return
                    }
            }
            if let result = response.value?.first {
                success(result)
            } else {
                failure()
            }
        }
    }
}
