//
//  Header.swift
//  NYCSchools
//
//  Created by Sai Madhukar Somu on 6/16/22.
//

import Foundation
import UIKit

class HeaderView: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureHeader()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureHeader() {
        
        let gradient = UIImage.gradientImage(bounds: self.bounds, colors: [.systemBlue, .systemRed])
       
        let label = UILabel(frame: CGRect(x: 16, y: 16, width: self.bounds.width - 32, height: self.bounds.height - 32))
        label.center = self.center
        label.textAlignment = .center
        label.text = "BEST NYC HIGH SCHOOLS"
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.textColor = UIColor(patternImage: gradient)
        addSubview(label)
    }
}
