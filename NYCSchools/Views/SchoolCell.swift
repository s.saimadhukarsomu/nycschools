//
//  SchoolCell.swift
//  NYCSchools
//
//  Created by Sai Madhukar Somu on 6/16/22.
//

import UIKit

class SchoolCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var phone: UIButton!
    
    private var school: School?

    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateUI(school: School) {
        
        self.school = school
        name.text = school.name
        location.text = school.address1 + ", " + school.city + ", " + school.state + ", " + school.zip
        phone.setTitle(school.phone, for: .normal)
    }
    
    @IBAction func dialPhoneNumber() {
        
        guard let phone = school?.phone else { return }
        if let url = NSURL(string: "tel://\(phone)") as? URL, UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}
